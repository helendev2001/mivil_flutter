//Modelo para registrar un usuario con todos los datos

import 'dart:convert';

Register registerFromJson(String str) => Register.fromJson(json.decode(str));

String registerToJson(Register data) => json.encode(data.toJson());

class Register {
  String? email;
  String? name;
  String? firstName;
  String? password;
  String? mobile;
  bool? acceptEula;
  String? tenant;
  String? captcha;
  String? locale;

  Register({
    this.email,
    this.name,
    this.firstName,
    this.password,
    this.mobile,
    this.acceptEula,
    this.tenant,
    this.captcha,
    this.locale,
  });

  factory Register.fromJson(Map<String, dynamic> json) => Register(
        email: json["email"],
        name: json["name"],
        firstName: json["firstName"],
        password: json["password"],
        mobile: json["mobile"],
        acceptEula: json["acceptEula"],
        tenant: json["tenant"],
        captcha: json["captcha"],
        locale: json["locale"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "name": name,
        "firstName": firstName,
        "password": password,
        "mobile": mobile,
        "acceptEula": acceptEula,
        "tenant": tenant,
        "captcha": captcha,
        "locale": locale,
      };
}
