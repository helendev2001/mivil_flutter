//Modelo para el token

import 'dart:convert';

Token tokenFromJson(String str) => Token.fromJson(json.decode(str));

String tokenToJson(Token data) => json.encode(data.toJson());

class Token {
  Token({
    this.statusCode,
    this.token,
  });

  int? statusCode;
  String? token;

  factory Token.fromJson(Map<String, dynamic> json) => Token(
        statusCode: json["statusCode"],
        token: json["token"],
      );

  Map<String, dynamic> toJson() => {
        "statusCode": statusCode,
        "token": token,
      };
}
