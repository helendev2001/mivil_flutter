// To parse this JSON data, do
//
//     final charging = chargingFromJson(jsonString);
//Modelo con todos los datos de las estaciones de cargas

import 'dart:convert';

Charging chargingFromJson(String str) => Charging.fromJson(json.decode(str));

String chargingToJson(Charging data) => json.encode(data.toJson());

class Charging {
  final int? count;
  final List<Result>? result;
  final List<String>? projectFields;
  final bool? canListCompanies;
  final bool? canListSites;
  final bool? canListSiteAreas;
  final bool? canListUsers;
  final bool? canExport;

  Charging({
    this.count,
    this.result,
    this.projectFields,
    this.canListCompanies,
    this.canListSites,
    this.canListSiteAreas,
    this.canListUsers,
    this.canExport,
  });

  factory Charging.fromJson(Map<String, dynamic> json) => Charging(
        count: json["count"],
        result: json["result"] == null
            ? []
            : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
        projectFields: json["projectFields"] == null
            ? []
            : List<String>.from(json["projectFields"]!.map((x) => x)),
        canListCompanies: json["canListCompanies"],
        canListSites: json["canListSites"],
        canListSiteAreas: json["canListSiteAreas"],
        canListUsers: json["canListUsers"],
        canExport: json["canExport"],
      );

  Map<String, dynamic> toJson() => {
        "count": count,
        "result": result == null
            ? []
            : List<dynamic>.from(result!.map((x) => x.toJson())),
        "projectFields": projectFields == null
            ? []
            : List<dynamic>.from(projectFields!.map((x) => x)),
        "canListCompanies": canListCompanies,
        "canListSites": canListSites,
        "canListSiteAreas": canListSiteAreas,
        "canListUsers": canListUsers,
        "canExport": canExport,
      };
}

class Result {
  final dynamic chargeBoxSerialNumber;
  final String? chargePointModel;
  final dynamic chargePointSerialNumber;
  final String? chargePointVendor;
  final List<dynamic>? chargePoints;
  final List<Connector>? connectors;
  final List<double>? coordinates;
  final DateTime? createdOn;
  final String? firmwareVersion;
  final bool? issuer;
  final DateTime? lastReboot;
  final DateTime? lastSeen;
  final int? maximumPower;
  final String? ocppProtocol;
  final String? ocppVersion;
  final String? powerLimitUnit;
  final bool? public;
  final String? siteAreaId;
  final String? siteId;
  final int? voltage;
  final bool? inactive;
  final SiteArea? siteArea;
  final Site? site;
  final String? id;
  final bool? canRead;
  final bool? canListUsers;
  final bool? canAuthorize;
  final bool? canUpdate;
  final bool? canUpdateOcppParams;
  final bool? canLimitPower;
  final bool? canDeleteChargingProfile;
  final bool? canGetOcppParams;
  final bool? canUpdateChargingProfile;
  final bool? canGetConnectorQrCode;
  final bool? canDelete;
  final bool? canReserveNow;
  final bool? canReset;
  final bool? canClearCache;
  final bool? canGetConfiguration;
  final bool? canChangeConfiguration;
  final bool? canSetChargingProfile;
  final bool? canGetCompositeSchedule;
  final bool? canClearChargingProfile;
  final bool? canGetDiagnostics;
  final bool? canUpdateFirmware;
  final bool? canRemoteStopTransaction;
  final bool? canStopTransaction;
  final bool? canStartTransaction;
  final bool? canChangeAvailability;
  final bool? canRemoteStartTransaction;
  final bool? canUnlockConnector;
  final bool? canDataTransfer;
  final bool? canGenerateQrCode;
  final bool? canMaintainPricingDefinitions;
  final bool? canListCompletedTransactions;

  Result({
    this.chargeBoxSerialNumber,
    this.chargePointModel,
    this.chargePointSerialNumber,
    this.chargePointVendor,
    this.chargePoints,
    this.connectors,
    this.coordinates,
    this.createdOn,
    this.firmwareVersion,
    this.issuer,
    this.lastReboot,
    this.lastSeen,
    this.maximumPower,
    this.ocppProtocol,
    this.ocppVersion,
    this.powerLimitUnit,
    this.public,
    this.siteAreaId,
    this.siteId,
    this.voltage,
    this.inactive,
    this.siteArea,
    this.site,
    this.id,
    this.canRead,
    this.canListUsers,
    this.canAuthorize,
    this.canUpdate,
    this.canUpdateOcppParams,
    this.canLimitPower,
    this.canDeleteChargingProfile,
    this.canGetOcppParams,
    this.canUpdateChargingProfile,
    this.canGetConnectorQrCode,
    this.canDelete,
    this.canReserveNow,
    this.canReset,
    this.canClearCache,
    this.canGetConfiguration,
    this.canChangeConfiguration,
    this.canSetChargingProfile,
    this.canGetCompositeSchedule,
    this.canClearChargingProfile,
    this.canGetDiagnostics,
    this.canUpdateFirmware,
    this.canRemoteStopTransaction,
    this.canStopTransaction,
    this.canStartTransaction,
    this.canChangeAvailability,
    this.canRemoteStartTransaction,
    this.canUnlockConnector,
    this.canDataTransfer,
    this.canGenerateQrCode,
    this.canMaintainPricingDefinitions,
    this.canListCompletedTransactions,
  });

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        chargeBoxSerialNumber: json["chargeBoxSerialNumber"],
        chargePointModel: json["chargePointModel"],
        chargePointSerialNumber: json["chargePointSerialNumber"],
        chargePointVendor: json["chargePointVendor"],
        chargePoints: json["chargePoints"] == null
            ? []
            : List<dynamic>.from(json["chargePoints"]!.map((x) => x)),
        connectors: json["connectors"] == null
            ? []
            : List<Connector>.from(
                json["connectors"]!.map((x) => Connector.fromJson(x))),
        coordinates: json["coordinates"] == null
            ? []
            : List<double>.from(json["coordinates"]!.map((x) => x?.toDouble())),
        createdOn: json["createdOn"] == null
            ? null
            : DateTime.parse(json["createdOn"]),
        firmwareVersion: json["firmwareVersion"],
        issuer: json["issuer"],
        lastReboot: json["lastReboot"] == null
            ? null
            : DateTime.parse(json["lastReboot"]),
        lastSeen:
            json["lastSeen"] == null ? null : DateTime.parse(json["lastSeen"]),
        maximumPower: json["maximumPower"],
        ocppProtocol: json["ocppProtocol"],
        ocppVersion: json["ocppVersion"],
        powerLimitUnit: json["powerLimitUnit"],
        public: json["public"],
        siteAreaId: json["siteAreaID"],
        siteId: json["siteID"],
        voltage: json["voltage"],
        inactive: json["inactive"],
        siteArea: json["siteArea"] == null
            ? null
            : SiteArea.fromJson(json["siteArea"]),
        site: json["site"] == null ? null : Site.fromJson(json["site"]),
        id: json["id"],
        canRead: json["canRead"],
        canListUsers: json["canListUsers"],
        canAuthorize: json["canAuthorize"],
        canUpdate: json["canUpdate"],
        canUpdateOcppParams: json["canUpdateOCPPParams"],
        canLimitPower: json["canLimitPower"],
        canDeleteChargingProfile: json["canDeleteChargingProfile"],
        canGetOcppParams: json["canGetOCPPParams"],
        canUpdateChargingProfile: json["canUpdateChargingProfile"],
        canGetConnectorQrCode: json["canGetConnectorQRCode"],
        canDelete: json["canDelete"],
        canReserveNow: json["canReserveNow"],
        canReset: json["canReset"],
        canClearCache: json["canClearCache"],
        canGetConfiguration: json["canGetConfiguration"],
        canChangeConfiguration: json["canChangeConfiguration"],
        canSetChargingProfile: json["canSetChargingProfile"],
        canGetCompositeSchedule: json["canGetCompositeSchedule"],
        canClearChargingProfile: json["canClearChargingProfile"],
        canGetDiagnostics: json["canGetDiagnostics"],
        canUpdateFirmware: json["canUpdateFirmware"],
        canRemoteStopTransaction: json["canRemoteStopTransaction"],
        canStopTransaction: json["canStopTransaction"],
        canStartTransaction: json["canStartTransaction"],
        canChangeAvailability: json["canChangeAvailability"],
        canRemoteStartTransaction: json["canRemoteStartTransaction"],
        canUnlockConnector: json["canUnlockConnector"],
        canDataTransfer: json["canDataTransfer"],
        canGenerateQrCode: json["canGenerateQrCode"],
        canMaintainPricingDefinitions: json["canMaintainPricingDefinitions"],
        canListCompletedTransactions: json["canListCompletedTransactions"],
      );

  Map<String, dynamic> toJson() => {
        "chargeBoxSerialNumber": chargeBoxSerialNumber,
        "chargePointModel": chargePointModel,
        "chargePointSerialNumber": chargePointSerialNumber,
        "chargePointVendor": chargePointVendor,
        "chargePoints": chargePoints == null
            ? []
            : List<dynamic>.from(chargePoints!.map((x) => x)),
        "connectors": connectors == null
            ? []
            : List<dynamic>.from(connectors!.map((x) => x.toJson())),
        "coordinates": coordinates == null
            ? []
            : List<dynamic>.from(coordinates!.map((x) => x)),
        "createdOn": createdOn?.toIso8601String(),
        "firmwareVersion": firmwareVersion,
        "issuer": issuer,
        "lastReboot": lastReboot?.toIso8601String(),
        "lastSeen": lastSeen?.toIso8601String(),
        "maximumPower": maximumPower,
        "ocppProtocol": ocppProtocol,
        "ocppVersion": ocppVersion,
        "powerLimitUnit": powerLimitUnit,
        "public": public,
        "siteAreaID": siteAreaId,
        "siteID": siteId,
        "voltage": voltage,
        "inactive": inactive,
        "siteArea": siteArea?.toJson(),
        "site": site?.toJson(),
        "id": id,
        "canRead": canRead,
        "canListUsers": canListUsers,
        "canAuthorize": canAuthorize,
        "canUpdate": canUpdate,
        "canUpdateOCPPParams": canUpdateOcppParams,
        "canLimitPower": canLimitPower,
        "canDeleteChargingProfile": canDeleteChargingProfile,
        "canGetOCPPParams": canGetOcppParams,
        "canUpdateChargingProfile": canUpdateChargingProfile,
        "canGetConnectorQRCode": canGetConnectorQrCode,
        "canDelete": canDelete,
        "canReserveNow": canReserveNow,
        "canReset": canReset,
        "canClearCache": canClearCache,
        "canGetConfiguration": canGetConfiguration,
        "canChangeConfiguration": canChangeConfiguration,
        "canSetChargingProfile": canSetChargingProfile,
        "canGetCompositeSchedule": canGetCompositeSchedule,
        "canClearChargingProfile": canClearChargingProfile,
        "canGetDiagnostics": canGetDiagnostics,
        "canUpdateFirmware": canUpdateFirmware,
        "canRemoteStopTransaction": canRemoteStopTransaction,
        "canStopTransaction": canStopTransaction,
        "canStartTransaction": canStartTransaction,
        "canChangeAvailability": canChangeAvailability,
        "canRemoteStartTransaction": canRemoteStartTransaction,
        "canUnlockConnector": canUnlockConnector,
        "canDataTransfer": canDataTransfer,
        "canGenerateQrCode": canGenerateQrCode,
        "canMaintainPricingDefinitions": canMaintainPricingDefinitions,
        "canListCompletedTransactions": canListCompletedTransactions,
      };
}

class Connector {
  final int? connectorId;
  final double? currentInstantWatts;
  final int? currentStateOfCharge;
  final int? currentTotalInactivitySecs;
  final double? currentTotalConsumptionWh;
  final String? currentTagId;
  final int? currentTransactionId;
  final String? status;
  final String? errorCode;
  final dynamic info;
  final dynamic vendorErrorCode;
  final int? power;
  final String? type;
  final User? user;
  final bool? canRemoteStopTransaction;
  final bool? canUnlockConnector;
  final bool? canReadTransaction;
  final bool? canRemoteStartTransaction;

  Connector({
    this.connectorId,
    this.currentInstantWatts,
    this.currentStateOfCharge,
    this.currentTotalInactivitySecs,
    this.currentTotalConsumptionWh,
    this.currentTagId,
    this.currentTransactionId,
    this.status,
    this.errorCode,
    this.info,
    this.vendorErrorCode,
    this.power,
    this.type,
    this.user,
    this.canRemoteStopTransaction,
    this.canUnlockConnector,
    this.canReadTransaction,
    this.canRemoteStartTransaction,
  });

  factory Connector.fromJson(Map<String, dynamic> json) => Connector(
        connectorId: json["connectorId"],
        currentInstantWatts: json["currentInstantWatts"]?.toDouble(),
        currentStateOfCharge: json["currentStateOfCharge"],
        currentTotalInactivitySecs: json["currentTotalInactivitySecs"],
        currentTotalConsumptionWh:
            json["currentTotalConsumptionWh"]?.toDouble(),
        currentTagId: json["currentTagID"],
        currentTransactionId: json["currentTransactionID"],
        status: json["status"],
        errorCode: json["errorCode"],
        info: json["info"],
        vendorErrorCode: json["vendorErrorCode"],
        power: json["power"],
        type: json["type"],
        user: json["user"] == null ? null : User.fromJson(json["user"]),
        canRemoteStopTransaction: json["canRemoteStopTransaction"],
        canUnlockConnector: json["canUnlockConnector"],
        canReadTransaction: json["canReadTransaction"],
        canRemoteStartTransaction: json["canRemoteStartTransaction"],
      );

  Map<String, dynamic> toJson() => {
        "connectorId": connectorId,
        "currentInstantWatts": currentInstantWatts,
        "currentStateOfCharge": currentStateOfCharge,
        "currentTotalInactivitySecs": currentTotalInactivitySecs,
        "currentTotalConsumptionWh": currentTotalConsumptionWh,
        "currentTagID": currentTagId,
        "currentTransactionID": currentTransactionId,
        "status": status,
        "errorCode": errorCode,
        "info": info,
        "vendorErrorCode": vendorErrorCode,
        "power": power,
        "type": type,
        "user": user?.toJson(),
        "canRemoteStopTransaction": canRemoteStopTransaction,
        "canUnlockConnector": canUnlockConnector,
        "canReadTransaction": canReadTransaction,
        "canRemoteStartTransaction": canRemoteStartTransaction,
      };
}

class User {
  final String? email;
  final String? firstName;
  final String? name;
  final String? id;

  User({
    this.email,
    this.firstName,
    this.name,
    this.id,
  });

  factory User.fromJson(Map<String, dynamic> json) => User(
        email: json["email"],
        firstName: json["firstName"],
        name: json["name"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "email": email,
        "firstName": firstName,
        "name": name,
        "id": id,
      };
}

class Site {
  final String? name;

  Site({
    this.name,
  });

  factory Site.fromJson(Map<String, dynamic> json) => Site(
        name: json["name"],
      );

  Map<String, dynamic> toJson() => {
        "name": name,
      };
}

class SiteArea {
  final Address? address;
  final String? name;
  final String? siteId;
  final String? id;

  SiteArea({
    this.address,
    this.name,
    this.siteId,
    this.id,
  });

  factory SiteArea.fromJson(Map<String, dynamic> json) => SiteArea(
        address:
            json["address"] == null ? null : Address.fromJson(json["address"]),
        name: json["name"],
        siteId: json["siteID"],
        id: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "address": address?.toJson(),
        "name": name,
        "siteID": siteId,
        "id": id,
      };
}

class Address {
  final String? address1;
  final String? address2;
  final String? postalCode;
  final String? city;
  final String? department;
  final String? region;
  final String? country;
  final List<double>? coordinates;

  Address({
    this.address1,
    this.address2,
    this.postalCode,
    this.city,
    this.department,
    this.region,
    this.country,
    this.coordinates,
  });

  factory Address.fromJson(Map<String, dynamic> json) => Address(
        address1: json["address1"],
        address2: json["address2"],
        postalCode: json["postalCode"],
        city: json["city"],
        department: json["department"],
        region: json["region"],
        country: json["country"],
        coordinates: json["coordinates"] == null
            ? []
            : List<double>.from(json["coordinates"]!.map((x) => x?.toDouble())),
      );

  Map<String, dynamic> toJson() => {
        "address1": address1,
        "address2": address2,
        "postalCode": postalCode,
        "city": city,
        "department": department,
        "region": region,
        "country": country,
        "coordinates": coordinates == null
            ? []
            : List<dynamic>.from(coordinates!.map((x) => x)),
      };
}
