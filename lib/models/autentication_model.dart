//Modelo para Autenticar al usuario
class Login {
  Login({
    required this.acceptEula,
    required this.email,
    required this.password,
    required this.tenant,
  });

  bool? acceptEula;
  String? email;
  String? password;
  String? tenant;
  String? token;

  factory Login.fromJson(Map<String, dynamic> json) => Login(
        acceptEula: json["acceptEula"],
        email: json["email"],
        password: json["password"],
        tenant: json["tenant"],
      );

  Map<String, dynamic> toJson() => {
        "acceptEula": acceptEula,
        "email": email,
        "password": password,
        "tenant": tenant,
      };
  factory Login.init() => Login(
        acceptEula: true,
        email: "",
        password: "",
        tenant: "",
      );
}
