//Modelo Marcador que incluye otros detalles aparte del marcador

import 'package:flutter_map/flutter_map.dart';
import 'package:mivilsoft/models/detallesCarga_model.dart';

class Marcador {
  Marker? marker;
  bool? activado;
  String? direccion;
  DetailsCharging? detalles;

  Marcador(Marker marker, bool activado /*, String direccion*/,
      DetailsCharging detalles) {
    this.marker = marker;
    this.activado = false;
    this.direccion = direccion;
    this.detalles = detalles;
  }
}
