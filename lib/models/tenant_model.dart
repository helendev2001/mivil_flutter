// To parse this JSON data, do
//
//     final tenant = tenantFromJson(jsonString);
//Modelo para obtener el nombre de la organizacion

import 'dart:convert';

Tenant tenantFromJson(String str) => Tenant.fromJson(json.decode(str));

String tenantToJson(Tenant data) => json.encode(data.toJson());

class Tenant {
    Tenant({
        this.count,
        this.result,
    });

    int? count;
    List<Result>? result;

    factory Tenant.fromJson(Map<String, dynamic> json) => Tenant(
        count: json["count"],
        result: json["result"] == null ? [] : List<Result>.from(json["result"]!.map((x) => Result.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "count": count,
        "result": result == null ? [] : List<dynamic>.from(result!.map((x) => x.toJson())),
    };
}

class Result {
    Result({
        this.createdBy,
        this.createdOn,
        this.email,
        this.lastChangedBy,
        this.lastChangedOn,
        this.name,
        this.subdomain,
        this.id,
    });

    CreatedBy? createdBy;
    DateTime? createdOn;
    String? email;
    dynamic lastChangedBy;
    DateTime? lastChangedOn;
    String? name;
    String? subdomain;
    String? id;

    factory Result.fromJson(Map<String, dynamic> json) => Result(
        createdBy: json["createdBy"] == null ? null : CreatedBy.fromJson(json["createdBy"]),
        createdOn: json["createdOn"] == null ? null : DateTime.parse(json["createdOn"]),
        email: json["email"],
        lastChangedBy: json["lastChangedBy"],
        lastChangedOn: json["lastChangedOn"] == null ? null : DateTime.parse(json["lastChangedOn"]),
        name: json["name"],
        subdomain: json["subdomain"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "createdBy": createdBy?.toJson(),
        "createdOn": createdOn?.toIso8601String(),
        "email": email,
        "lastChangedBy": lastChangedBy,
        "lastChangedOn": lastChangedOn?.toIso8601String(),
        "name": name,
        "subdomain": subdomain,
        "id": id,
    };
}

class CreatedBy {
    CreatedBy({
        this.authorizationId,
        this.firstName,
        this.name,
        this.id,
    });

    dynamic authorizationId;
    String? firstName;
    String? name;
    String? id;

    factory CreatedBy.fromJson(Map<String, dynamic> json) => CreatedBy(
        authorizationId: json["authorizationID"],
        firstName: json["firstName"],
        name: json["name"],
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "authorizationID": authorizationId,
        "firstName": firstName,
        "name": name,
        "id": id,
    };
}
