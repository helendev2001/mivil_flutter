// To parse this JSON data, do
//
//     final detailsCharging = detailsChargingFromJson(jsonString);
//Modelo de detalles especificos de las estaciones de carga

import 'dart:convert';

DetailsCharging detailsChargingFromJson(String str) =>
    DetailsCharging.fromJson(json.decode(str));

String detailsChargingToJson(DetailsCharging data) =>
    json.encode(data.toJson());

class DetailsCharging {
  String? siteAdress;
  String? chargePointModel;
  String? chargePointVendor;
  int? maximumPower;

  DetailsCharging({
    this.siteAdress,
    this.chargePointModel,
    this.chargePointVendor,
    this.maximumPower,
  });

  factory DetailsCharging.fromJson(Map<String, dynamic> json) =>
      DetailsCharging(
        siteAdress: json["siteAdress"],
        chargePointModel: json["chargePointModel"],
        chargePointVendor: json["chargePointVendor"],
        maximumPower: json["maximumPower"],
      );

  Map<String, dynamic> toJson() => {
        "siteAdress": siteAdress,
        "chargePointModel": chargePointModel,
        "chargePointVendor": chargePointVendor,
        "maximumPower": maximumPower,
      };
}
