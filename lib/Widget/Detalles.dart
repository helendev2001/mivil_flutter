import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geocoding/geocoding.dart';
import 'package:latlong2/latlong.dart';
import 'package:mivilsoft/models/charging_model.dart';
import 'package:mivilsoft/models/detallesCarga_model.dart';
import 'package:mivilsoft/models/marker_model.dart';

class Detalles extends StatefulWidget {
  final Marker marker;
  final List<Marcador> marcador;
  final Function(Marcador) changeColor;
  final Placemark resultado;
  final List<Result> listCharges;
  

  const Detalles(this.changeColor, this.marcador, this.marker, this.resultado,
      this.listCharges,
      {super.key});

  @override
  State<Detalles> createState() => _DetallesState();
}

class _DetallesState extends State<Detalles> {
  final geocoding = GeocodingPlatform.instance;
  Orientation orientation = Orientation.portrait;
  List<String> detalles = [];
  List<String> connectores = [];
  LatLng? point;

  Size size = const Size(0, 0);
  double letter = 0;

  bool activar = false;

  @override
  void initState() {
    // Inicialización de la pantalla de detalles
    super.initState();
    //Obtenemos los marcadores seleccionados
    for (Marcador m in widget.marcador) {
      if (m.marker!.point == widget.marker.point) {
        activar = m.activado!;
        break;
      }
    }
    cargarDetalles();
  }

  //Recorre las estaciones de carga para obtener los detalles
  //Al final recarga los datos con el setState
  cargarDetalles() async {
    setState(() {});
    if (widget.listCharges.isNotEmpty) {
      for (Result r in widget.listCharges) {
        if (r.coordinates != null &&
            r.coordinates!.isNotEmpty &&
            r.coordinates!.length == 2) {
          point = LatLng(r.coordinates!.last, r.coordinates!.first);

          if (point == widget.marker.point) {
            detalles.add(r.chargePointModel!);
            detalles.add(r.chargePointVendor!);
            detalles.add(r.connectors!.length.toString());
            detalles.add(r.maximumPower!.toString());
            detalles.add(r.siteArea!.address!.address1.toString());
            for (Connector c in r.connectors!) {
              connectores.add("       " +
                  c.connectorId.toString() +
                  " \nEstado: " +
                  c.status.toString() +
                  " \nEnergia: " +
                  c.power.toString() +
                  " \nTipo de Conector: " +
                  c.type.toString());
            }
          }
        }
      }
    }
    setState(() {
      //notifica que el estado ha cambiado
      detalles;
      connectores;
    });
  }

//Se crea el logo de la empresa
//Este método devuelve un widget que muestra el logo de la empresa en la parte superior de la pantalla de detalles.
  Widget _logo() {
    return Positioned(
        top: (size.height / 15),
        left: 0,
        right: 0,
        child: Image.asset(
          'assets/images/logo_sin_fondo.png',
          width: orientation == Orientation.portrait ? 100 : 50,
          height: orientation == Orientation.portrait ? 100 : 50,
          //scale: 1.5,
        ));
  }

  /// Widget que muestra un icono de ubicación en la pantalla
  Widget _icono() {
    return Positioned(
        top: orientation == Orientation.portrait
            ? (size.height / 5)
            : (size.height / 4),
        left: 0,
        right: 0,
        child: Image.asset(
          'assets/images/icons8-marcador-128.png',
          width: orientation == Orientation.portrait ? 200 : size.width / 10,
          height: orientation == Orientation.portrait ? 200 : size.width / 10,
          //scale: 1.5,
        ));
  }

//Obtiene el pais del marcador
  Widget _pais() {
    return Positioned(
        top: orientation == Orientation.portrait
            ? (size.height / 2.2)
            : (size.height / 2.3),
        left: 0,
        right: 0,
        child: Text(
          widget.resultado.country.toString(),
          textAlign: TextAlign.center,
          style:
              TextStyle(color: Colors.black, fontSize: letter >= 1 ? 20 : 30),
        ));
  }

// Ubicacion detallada de los marcadores
  Widget _detalleAdress() {
    return Positioned(
        //bottom: 50,
        top: orientation == Orientation.portrait
            ? (size.height / 2) + 50
            : (size.height / 2) + 50,
        left: orientation == Orientation.portrait ? 60 : 30,
        right: 0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                const Text(
                  "Detalle",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                const SizedBox(
                  width: 50,
                ),
                Text(
                  " Provincia: " +
                      widget.resultado.administrativeArea.toString() +
                      "\n Canton: " +
                      widget.resultado.subAdministrativeArea.toString() +
                      " \n Modelo: " +
                      detalles.elementAt(0) +
                      "\n Proveedor: " +
                      detalles.elementAt(1) +
                      " \n Número de conectores: " +
                      detalles.elementAt(2) +
                      " \n Potencia máxima: " +
                      detalles.elementAt(3) +
                      " \n Ubicación: " +
                      detalles.elementAt(4),
                  textAlign: TextAlign.justify,
                  style: TextStyle(color: Colors.black, fontSize: 12),
                ),
              ],
            ),
            orientation == Orientation.portrait
                ? Divider(
                    height: 20,
                    endIndent: 60,
                    thickness: 2,
                  )
                : Row()
          ],
        ));
  }

  //Informacion detallada de las estaciones de carga
  Widget _detalleConectores() {
    return Positioned(
        top: orientation == Orientation.portrait
            ? (size.height / 2) + 200
            : (size.height / 2) + 40,
        left: orientation == Orientation.portrait ? 60 : 330,
        right: 0,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              children: [
                const Text(
                  "Conectores",
                  textAlign: TextAlign.center,
                  style: TextStyle(color: Colors.black, fontSize: 15),
                ),
                const SizedBox(
                  width: 50,
                ),
                Text(
                  connectores
                      .toString()
                      .replaceAll(',', '\n\n')
                      .replaceAll('[', '')
                      .replaceAll(']', ''),
                  textAlign: TextAlign.justify,
                  style: TextStyle(color: Colors.black, fontSize: 12),
                ),
              ],
            ),
            orientation == Orientation.portrait
                ? Divider(
                    height: 20,
                    endIndent: 60,
                    thickness: 2,
                  )
                : Row()
          ],
        ));
  }

//Widget principal donde se contruyen todos los anteriores widgets
  @override
  Widget build(BuildContext context) {
    size = MediaQuery.of(context).size;
    letter = MediaQuery.of(context).textScaleFactor;
    orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      body: Stack(
        children: [
          Positioned(
            top: 0,
            left: 0,
            right: 0,
            bottom: size.height / 2,
            child: new Container(
              //height: size.height / 2,
              decoration: const BoxDecoration(
                  gradient: const LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [Colors.blue, Colors.blue])),
            ),
          ),
          _logo(),
          _icono(),
          _pais(),
          _detalleAdress(),
          _detalleConectores()
        ],
      ),
      //Boton inferior
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          for (Marcador m in widget.marcador) {
            if (m.marker!.point == widget.marker.point) {
              widget.changeColor(m);
              setState(() {
                activar = m.activado!;
              });
            }
          }
        },
        backgroundColor: activar ? Colors.red : Colors.green,
        icon: activar
            ? Icon(Icons.refresh)
            : Image.asset(
                'assets/images/icons8-reserva-2-30.png'), //Icon(Icons.refresh),

        label: activar
            ? const Text(
                'Cancelar',
                style: TextStyle(fontSize: 20, color: Colors.white),
              )
            : const Text(
                'Reservar',
                style: TextStyle(fontSize: 20, color: Colors.white),
              ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }
}
