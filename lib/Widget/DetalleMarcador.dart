import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:geocoding/geocoding.dart';
import 'package:latlong2/latlong.dart';
import 'package:mivilsoft/models/charging_model.dart';
import 'package:mivilsoft/models/detallesCarga_model.dart';

import '../models/marker_model.dart';
import 'Detalles.dart';

class ExamplePopup2 extends StatefulWidget {
  final Marker marker;
  final List<Marcador> marcador;
  final MapController mapController;
  final Function(Marcador) changeColor;
  final Placemark resultado;
  final bool adress;
  final String token;
  final List<String> direccion;
  final List<Result> listCharges;
  final String direc;
  final DetailsCharging detalles;

  const ExamplePopup2(
      this.marker,
      this.mapController,
      this.changeColor,
      this.marcador,
      this.resultado,
      this.adress,
      this.token,
      this.listCharges,
      this.direccion,
      this.direc,
      this.detalles,
      {Key? key})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ExamplePopup2State();
}

class _ExamplePopup2State extends State<ExamplePopup2> {
//Variables
  double sizeletter = 0;
  List<Result>? listCharges;
  List<String>? detalles;
  LatLng? point;

  @override
  void initState() {
    super.initState();
  }

  // Calcula el tamaño del widget del cuadro de descripción en función de la orientación de la pantalla y el tamaño de la fuente.
  List<double> _orientation(double letter, Orientation orientation) {
    List<double> _cardSize = [];
    if (orientation == Orientation.landscape) {
      _cardSize.add(letter / 4);
      _cardSize.add(letter / 5.5);
    } else {
      _cardSize.add(letter / 2.5);
      _cardSize.add(letter / 10);
    }
    return _cardSize;
  }

  // Widget que muestra la descripción del marcador y permite la navegación a los detalles.
  Widget _cardDescription(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final letter = MediaQuery.of(context).textScaleFactor;
    final orientation = MediaQuery.of(context).orientation;
    return InkWell(
      onTap: () {
        if (widget.adress == true) {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => Detalles(
                      widget.changeColor,
                      widget.marcador,
                      widget.marker,
                      widget.resultado,
                      widget.listCharges)));
        } else {
          final snackBar = SnackBar(
            content: const Text('¡Posición incorrecta!'),
            action: SnackBarAction(
              label: 'Ok',
              onPressed: () {},
            ),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      },
      child: Container(
        width: size.width * _orientation(letter, orientation).first,
        height: size.height * _orientation(letter, orientation).last * 1.1,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'Información',
              overflow: TextOverflow.fade,
              textAlign: TextAlign.center,
              style: TextStyle(
                fontWeight: FontWeight.w500,
                fontSize: 12,
              ),
            ),
            Text(
              'Posición:',
              style: TextStyle(
                fontSize: 12,
                fontWeight: FontWeight.w500,
              ),
            ),
            Expanded(
              flex: 2,
              child: Text(
                  softWrap: false,
                  '   Latitud: ${widget.marker.point.latitude}\n   Longitud: ${widget.marker.point.longitude}\n   Dirección: ${widget.detalles.siteAdress} \n   Modelo: ${widget.detalles.chargePointModel} \n   Proveedor: ${widget.detalles.chargePointVendor} \n   Energia: ${widget.detalles.maximumPower.toString()}' +
                      ' kW',
                  style: TextStyle(fontSize: 10),
                  textAlign: TextAlign.justify),
            )
          ],
        ),
      ),
    );
  }

//Widget principal donde se contruyen todos los anteriores widgets
  @override
  Widget build(BuildContext context) {
    return Card(
      child: InkWell(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 5, right: 5),
            ),
            _cardDescription(context),
          ],
        ),
      ),
    );
  }
}
