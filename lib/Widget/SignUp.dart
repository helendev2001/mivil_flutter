import 'package:flutter/material.dart';
import 'package:mivilsoft/Widget/LoginPage.dart';
import 'package:mivilsoft/Widget/WelcomePage.dart';
import 'package:mivilsoft/models/register_model.dart';
import 'package:mivilsoft/models/tenant_model.dart';
import 'package:mivilsoft/services/register.dart';
import 'package:mivilsoft/services/tenant.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';
import '../services/verify_token.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
//import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:progress_dialog2/progress_dialog2.dart';

class SignUp extends StatefulWidget {
  const SignUp({super.key});

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  //ProgressDialog _progressDialog = ProgressDialog();
  String? token;
  Register usuario = Register();
  List<Result>? listTenants;
  List<String> listSudomain = [];
  String? selectedCombo;
  String? sudomain;
  bool verify = false;
  bool isChecked = false;
  bool loading = true;
  bool passwordValid = true;
  bool _obscureText = true;
  late ProgressDialog pr;
  RegExp passwordRegex = RegExp(
      r'^(?=.*[A-Z])(?=.*\d)(?=.*[\W_]).+$'); // Expresión regular para validar la contraseña
  //Esta función se llama cuando se inicia el estado del widget y se encarga de realizar algunas tareas iniciales.
  void initState() {
    super.initState();
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Cargando...');
    pr.show();
    /*_progressDialog.showProgressDialog(context,
        dismissAfter: Duration(seconds: 5),
        textToBeDisplayed: 'Cargando datos...');
         pr.show();*/
    //Obtiene los dominios
    cargarTenants();
  }

  //Funciones
  //Esta función se utiliza para obtener los dominios
  cargarTenants() {
    getTenant().then((value) async {
      listTenants = value;
      if (listTenants != null) {
        for (Result r in listTenants!) {
          sudomain = r.subdomain.toString();
          listSudomain.add(sudomain!);
        }
      }
      setState(() {
        listSudomain;
      });
      await Future.delayed(Duration(seconds: 10));
      pr.hide();
      // _progressDialog.dismissProgressDialog(context);
    });
  }

//Esta función se utiliza para verificar un token y muestra un diálogo informativo en función del resultado
  void verifyToken(String token) {
    validateCaptcha(token).then((value) async {
      verify = value;
      setState(() {
        verify;
      });
      if (verify) {
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Recaptcha"),
                content: Text('Recaptcha Correcto'),
                actions: [
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            });
      } else {
        showDialog<void>(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("Recaptcha"),
                content: Text('Recaptcha no verificado'),
                actions: [
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Ok'),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => SignUp()));
                    },
                  ),
                ],
              );
            });
      }
      await Future.delayed(Duration(seconds: 10));
    });
  }

  //Mensaje de notificacion al tener un registro exitoso
  Future<void> mensajeRegistro() {
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Notificación"),
            content: Text("Usuario registrado exitosamente!!"),
            actions: [
              TextButton(
                style: TextButton.styleFrom(
                  textStyle: Theme.of(context).textTheme.labelLarge,
                ),
                child: const Text('Ok'),
                onPressed: () {
                  //Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => WelcomePage()));
                },
              ),
            ],
          );
        });
  }

//Se crea el logo de la empresa
//Este método devuelve un widget que muestra el logo de la empresa en la parte superior de la pantalla de detalles.
  Widget _logo() {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 5, 0, 0),
        alignment: Alignment.center,
        // Alto del contenedor
        child: Stack(children: [
          Positioned(
            child: Image.asset(
              'assets/images/logo_sin_fondo.png',
              width: 250,
              height: 250,
            ),
          ),
        ]));
  }

//Widget de una linea divisora
  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
      //margin: EdgeInsets.symmetric(vertical: 5),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 0),
              child: Divider(
                thickness: 2,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

//Boton para retroceder
  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => WelcomePage()));
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.white),
            ),
            Text('Back',
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                    fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

//Widget que crea un boton para enviar todo el formulario al servicio de registrar usaurio
  Widget _submitButton() {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: Color.fromARGB(255, 108, 175, 226), // background
        minimumSize: Size(350, 50),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ), // foreground
      ),
      onPressed: () async {
        int statusCode = await registerUser(usuario);
        print(statusCode.toString());
        if (passwordRegex.hasMatch(usuario.password.toString())) {
          if (statusCode == 200) {
            mensajeRegistro();
          }
        } else {
          passwordValid = false;
          setState(() {});
        }
      },
      child: Text('Registrar',
          style: TextStyle(fontSize: 20, color: Colors.white)),
    );
  }

//Este widget proporciona un enlace para ir a la página de inicio de sesión.
  Widget _loginAccountLabel() {
    return WillPopScope(
      onWillPop: () async {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => WelcomePage()));
        return false;
      },
      child: InkWell(
        onTap: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => LoginPage()));
        },
        child: Container(
          margin: EdgeInsets.symmetric(vertical: 20, horizontal: 40),
          alignment: Alignment.bottomCenter,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Tienes una cuenta creada?',
                style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                'Iniciar Sesión',
                style: TextStyle(
                    color: Color.fromARGB(255, 39, 106, 173),
                    fontSize: 15,
                    fontWeight: FontWeight.w600),
              ),
            ],
          ),
        ),
      ),
    );
  }

//Este widget devuelve una columna que contiene varios campos de entrada de usuario para el registro.
  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        _entryField("Correo Electrónico", 'icons8-cuenta-de-correo-30.png'),
        _entryField("Usuario", 'icons8-usuario-30.png'),
        _entryField("Nombre", 'icons8-cuenta-de-correo-30.png'),
        _entryField("Contraseña", 'icons8-bloquear-30.png', isPassword: true),
        if (passwordValid == false)
          _alertaCampo(
              "La contraseña debe contener al menos una letra mayúscula, al menos un carácter especial y al menos un número. Debe tener mas de 8 caracteres"),
        _entryField("Teléfono", 'icons8-cuenta-de-correo-30.png'),
        comboTenants(),
        captcha()
      ],
    );
  }

//Esta variable es un mapa que contiene controladores de texto para diferentes campos del formulario de registro.
//Los controladores se utilizan para acceder a los valores ingresados por el usuario en estos campos.
  Map<String, TextEditingController> controllerMap = {
    'Email': TextEditingController(),
    'Name': TextEditingController(),
    'FirstName': TextEditingController(),
    'Password': TextEditingController(),
  };
//Widget que se encarga de crear un campo de entrada de texto con un título,
//una imagen de prefijo y la capacidad de ocultar o mostrar la contraseña según sea necesario.
  Widget _entryField(String title, String imagen, {bool isPassword = false}) {
    String enlace = 'assets/images/';
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
              controller: controllerMap[title],
              obscureText: isPassword == false ? isPassword : _obscureText,
              decoration: InputDecoration(
                  prefixIcon: Image.asset(enlace + imagen),
                  suffixIcon: isPassword == true
                      ? GestureDetector(
                          onTap: () {
                            setState(() {
                              _obscureText = !_obscureText;
                            });
                          },
                          child: Icon(
                            _obscureText
                                ? Icons.visibility
                                : Icons.visibility_off,
                          ),
                        )
                      : null,
                  border: InputBorder.none,
                  fillColor: Color(0xfff3f3f4),
                  filled: true),
              onChanged: (value) {
                switch (title) {
                  case 'Correo Electrónico':
                    usuario.email = value;
                    break;
                  case 'Usuario':
                    usuario.name = value;
                    break;
                  case 'Nombre':
                    usuario.firstName = value;
                    break;
                  case 'Contraseña':
                    usuario.password = value;
                    break;
                  case 'Teléfono':
                    usuario.mobile = value;
                    break;
                  default:
                    break;
                }
              })
        ],
      ),
    );
  }

//Widget creado para mostrar si el texto ingresado como contraseña es o no correcta
  Widget _alertaCampo(String texto) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            texto,
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 10, color: Colors.red),
          ),
        ],
      ),
    );
  }

//ComboBox con todos los dominios
  Widget comboTenants() {
    try {
      return Container(
        margin: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Dominio",
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
            SizedBox(
              height: 10,
            ),
            DropdownButtonHideUnderline(
              child: DropdownButton2(
                alignment: Alignment.center,
                isExpanded: true,
                buttonHeight: 50,
                buttonWidth: 220,
                buttonPadding: const EdgeInsets.symmetric(horizontal: 20),
                buttonDecoration: BoxDecoration(
                  color: Colors.white,
                ),
                hint: Row(
                  children: const [
                    Icon(
                      Icons.list,
                      size: 16,
                      color: Colors.black,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    Expanded(
                      child: Text(
                        'Select ',
                        style: TextStyle(
                          fontSize: 14,
                          //fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
                items: listSudomain
                    .map((item) => DropdownMenuItem<String>(
                          value: item,
                          child: Text(
                            item,
                            style: const TextStyle(
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ))
                    .toList(),
                value: selectedCombo,
                onChanged: (value) {
                  setState(() {
                    selectedCombo = value as String;
                    usuario.tenant = selectedCombo;
                  });
                },
                icon: const Icon(
                  Icons.arrow_forward_ios_outlined,
                  size: 15,
                  color: Color.fromARGB(255, 0, 0, 0),
                ),
                itemHeight: 40,
                dropdownPadding: EdgeInsets.symmetric(horizontal: 20),
                dropdownMaxHeight: 200,
                dropdownWidth: 200,
                dropdownDecoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(14),
                  color: Color.fromARGB(255, 142, 176, 250),
                ),
                scrollbarThickness: 10,
                scrollbarAlwaysShow: false,
              ),
            ),
          ],
        ),
      );
    } catch (e) {
      return AlertDialog();
    }
  }

//Widget donde crea todo el captcha como seguridad de ingreso
  Widget captcha() {
    return CheckboxListTile(
      title: Text(
        "Acepto el Acuerdo de Licencia de usuario final",
        style: TextStyle(fontSize: 13),
      ),

      controlAffinity: ListTileControlAffinity.leading,
      checkColor: Colors.white,
      //fillColor: MaterialStateProperty.resolveWith(Colors.yellow),
      value: isChecked,
      onChanged: (bool? value) {
        setState(() {
          isChecked = value!;
        });
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (context) => Scaffold(
              appBar: AppBar(
                title: Text('Captcha'),
              ),
              body: WebViewPlus(
                javascriptMode: JavascriptMode.unrestricted,
                onWebViewCreated: (controller) {
                  controller.loadUrl("assets/webpages/index.html");
                },
                javascriptChannels: Set.from([
                  JavascriptChannel(
                    name: 'Captcha',
                    onMessageReceived: (JavascriptMessage message) {
                      String token = message.message;
                      if (token != null) {
                        showDialog<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Recaptcha"),
                                content: Text('Captcha Generado'),
                                actions: [
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      textStyle: Theme.of(context)
                                          .textTheme
                                          .labelLarge,
                                    ),
                                    child: const Text('Ok'),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            });
                      } else {
                        showDialog<void>(
                            context: context,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("Recaptcha"),
                                content: Text('Verifique es un usuario'),
                                actions: [
                                  TextButton(
                                    style: TextButton.styleFrom(
                                      textStyle: Theme.of(context)
                                          .textTheme
                                          .labelLarge,
                                    ),
                                    child: const Text('Ok'),
                                    onPressed: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => SignUp()));
                                    },
                                  ),
                                ],
                              );
                            });
                      }
                      usuario.captcha = token;
                    },
                  ),
                ].toSet()),
              ),
            ),
          ),
        );
      },
    );
  }

//Widget principal donde carga todos los widgets
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
        body: Container(
      child: Container(
          height: size.height,
          width: size.width,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(5)),
              boxShadow: <BoxShadow>[
                BoxShadow(
                    color: Colors.grey.shade200,
                    offset: Offset(2, 4),
                    blurRadius: 5,
                    spreadRadius: 2)
              ],
              gradient: const LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.center,
                  colors: [
                    Color.fromRGBO(44, 62, 80, 0.75),
                    Color.fromARGB(255, 219, 230, 238)
                  ])),
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      _logo(),
                      _emailPasswordWidget(),
                      SizedBox(
                        height: 20,
                      ),
                      _submitButton(),
                      _divider(),
                      _loginAccountLabel(),
                    ],
                  ),
                ),
              ),
            ],
          )),
    ));
  } //);
}
//}
