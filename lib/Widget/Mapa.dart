//import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:progress_dialog2/progress_dialog2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/flutter_map.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:latlong2/latlong.dart';
import 'package:geocoding/geocoding.dart';
import 'package:mivilsoft/Widget/DetalleMarcador.dart';
import 'package:mivilsoft/Widget/WelcomePage.dart';
import 'package:mivilsoft/models/charging_model.dart';
import 'package:mivilsoft/models/detallesCarga_model.dart';
import 'package:mivilsoft/services/Carga.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/marker_model.dart';

class Mapa2 extends StatefulWidget {
  //final List<Marker> carga;
  final String token;
  const Mapa2(this.token, {super.key});

  @override
  State<Mapa2> createState() => _Mapa2State();
}

class _Mapa2State extends State<Mapa2> {
  final PopupController _popupLayerController = PopupController();
  final _geolocator = GeocodingPlatform.instance;
  double zoomLevel = 1;
  String direccion = '';
  List<String> direcciones = [];
  SharedPreferences? dataLogin;
  MapController _mapController = MapController();
  // ProgressDialog _progressDialog = ProgressDialog();
  Placemark _address = Placemark();
  bool loading = true;
  List<Marker> markers = [];
  List<Marcador> marcadores = [];
  Marcador? marcador;
  LatLng promedio = LatLng(-2.75, -78.50);
  LatLng? pointMarkers;
  List<Result>? listCharges;
  List<String> connectors = [];

  String modelo = '';
  String proveedor = '';
  int energia = 0;
  List<DetailsCharging> detallesCarga = [];
  late ProgressDialog pr;

  @override
  void initState() {
    // realizan varias tareas relacionadas con la inicialización de la página,
    // la carga de datos y la gestión de marcadores en un mapa.
    super.initState();
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Cargando...');
    pr.show();
    /*_progressDialog.showProgressDialog(context,
        dismissAfter: Duration(seconds: 5),
        textToBeDisplayed: 'Cargando datos...');*/
    _mapController = MapController();
    cargarCoordinates();
  }

//Esta función se encarga de cargar las coordenadas de carga y configurar los marcadores en el mapa.
  cargarCoordinates() {
    getDataCharge(widget.token).then((value) async {
      marcadores.clear();
      markers.clear();
      detallesCarga.clear();

      //DetailsCharging detalles = DetailsCharging();
      listCharges = value;
      if (listCharges != null) {
        for (Result r in listCharges!) {
          if (r.coordinates != null &&
              r.coordinates!.isNotEmpty &&
              r.coordinates!.length == 2) {
            DetailsCharging detalles = DetailsCharging();
            direccion = r.siteArea!.address!.address1.toString();
            direcciones.add(direccion);
            detalles.siteAdress =
                direccion = r.siteArea!.address!.address1.toString();
            detalles.chargePointModel = r.chargePointModel.toString();
            detalles.chargePointVendor = r.chargePointVendor.toString();
            detalles.maximumPower = r.maximumPower;
            detallesCarga.add(detalles);
            connectors.clear();
            for (Connector c in r.connectors!) {
              connectors.add(c.status.toString());
            }
            MaterialColor colores = colorMarker(connectors);
            markers.add(Marker(
                point: LatLng(r.coordinates!.last, r.coordinates!.first),
                builder: (_) => Image.asset(
                      color: colores,
                      'assets/images/icons8-marcador-96.png',
                    )));
          }
        }
      }
      setState(() {
        markers;
        promedio = _centrarMapa(markers);
      });
      int i = 0;
      for (Marker m in markers) {
        marcadores.add(Marcador(m, false,
            /*direcciones.elementAt(i),*/ detallesCarga.elementAt(i)));
        print("indice" + i.toString());
        i = i + 1;
      }

      pr.hide();
      await Future.delayed(Duration(seconds: 10));
    });

    //_progressDialog.dismissProgressDialog(context);
  }

//Esta función calcula el color del marcador en función del estado de los conectores
  MaterialColor colorMarker(List<String> connectors) {
    double a = 0, i = 0, ch = 0;
    MaterialColor color = Colors.grey;
    for (String s in connectors) {
      switch (s) {
        case 'Available':
          a = a + 1;
          break;
        case 'Unavailable':
          i = i + 1;
          break;
        case 'Charging':
          ch = ch + 1;
          break;
      }
    }
    if (a == connectors.length) {
      color = Colors.green;
    } else if (i == connectors.length) {
      color = Colors.red;
    } else if (ch == connectors.length) {
      color = Colors.blue;
    } else if (a == (connectors.length / 2) && i == (connectors.length / 2)) {
      color = Colors.green;
    } else if (a == (connectors.length / 2) && ch == (connectors.length / 2)) {
      color = Colors.blue;
    } else if (i == (connectors.length / 2) && ch == (connectors.length / 2)) {
      color = Colors.blue;
    }
    return color;
  }

//Este método se llama cuando se realiza un gesto de actualización
  Future<void> _handleRefresh() async {
    // Aquí se realiza las operaciones de recarga de datos
    setState(() {
      //direcciones = [];
      //detallesCarga = [];
    });
    cargarCoordinates();
  }

  // Esta función obtiene información de geolocalización para un marcador específico.
  Future<Placemark> _getPlace(Marker _position) async {
    Placemark placeMark = Placemark();
    try {
      List<Placemark> newPlace = await _geolocator.placemarkFromCoordinates(
          _position.point.latitude, _position.point.longitude,
          localeIdentifier: 'es');
      // this is all you need
      placeMark = newPlace[0];
      loading = true;
    } catch (e) {
      e.toString();
      loading = false;
    }

    setState(() {
      _address = placeMark; // update _address
    });
    return _address;
  }

//Esta función cambia el color del marcador en respuesta a una acción del usuario.
  void changeColor(Marcador? marcador) {
    Marcador? mark = marcador;

    //_markers.removeWhere((element) => element == m);
    if (mark!.activado!) {
      mark.marker = Marker(
          point: mark.marker!.point,
          builder: (_) => Image.asset(
                color: Colors.green,
                'assets/images/icons8-marcador-96.png',
              ));
      mark.activado = !mark.activado!;
    } else {
      mark.marker = Marker(
          point: mark.marker!.point,
          builder: (_) => Image.asset(
                color: Colors.red,
                'assets/images/icons8-marcador-96.png',
              ));
      mark.activado = !mark.activado!;
    }
    markers.clear();
    for (Marcador m in marcadores) {
      markers.add(m.marker!);
    }

    detallesEstacionesCarga = marcador!.detalles;
    setState(() {});
  }

//Esta función calcula la ubicación central del mapa en función de un conjunto de marcadores.
  LatLng _centrarMapa(List<Marker> _markers) {
    double sumLat = 0, sumLng = 0, promLat = 0, promLng = 0;
    for (Marker marker in _markers) {
      sumLat = sumLat + marker.point.latitude;
      sumLng = sumLng + marker.point.longitude;
    }
    promLat = sumLat / _markers.length;
    promLng = sumLng / _markers.length;
    return LatLng(promLat, promLng);
  }

//Esta función se encarga de calcular el nivel de zoom del mapa.
  double _zoomMapa() {
    if (markers.length > 1) {
      List<LatLng> markerPoints =
          markers.map((marker) => marker.point).toList();
      LatLngBounds bounds = LatLngBounds.fromPoints(markerPoints);

      _mapController.fitBounds(bounds,
          options: FitBoundsOptions(padding: EdgeInsets.all(50)));
      zoomLevel = _mapController.zoom;
    }
    return zoomLevel;
  }

//Esta función se llama cuando el usuario intenta cerrar la aplicación (por ejemplo, presionando el botón de retroceso del dispositivo).
//Muestra un cuadro de diálogo de confirmación y devuelve un
//valor booleano (true si el usuario elige cerrar la aplicación y false si elige quedarse).
  Future<bool> _onWillPopScope(BuildContext context) async {
    return (await showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text("¿Seguro deseas cerrar sesión"),
                content: Text('Vas a salir de la aplicación '),
                actions: [
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('Si'),
                    onPressed: () {
                      //Navigator.of(context).pop(true);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => WelcomePage()));
                    },
                  ),
                  TextButton(
                    style: TextButton.styleFrom(
                      textStyle: Theme.of(context).textTheme.labelLarge,
                    ),
                    child: const Text('No '),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            })) ??
        false;
  }

  DetailsCharging? detallesEstacionesCarga = DetailsCharging();
  //Widgets
//Este es un widget que dibuja el mapa y sus marcadores.
  Widget _mapa() {
    return Container(
        child: FlutterMap(
      mapController: _mapController,
      options: MapOptions(
        zoom: 6.7, //_zoomMapa(),
        center: LatLng(-1.7572704449328285, -77.95348717933551), // promedio,
        //Makes the map and marker not rotate
        interactiveFlags: InteractiveFlag.all & ~InteractiveFlag.rotate,
      ),
      children: [
        TileLayer(
          urlTemplate: "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
          subdomains: ['a', 'b', 'c'],
        ),
        PopupMarkerLayerWidget(
          options: PopupMarkerLayerOptions(
              popupController: _popupLayerController,
              markers: markers,
              markerRotateAlignment:
                  PopupMarkerLayerOptions.rotationAlignmentFor(AnchorAlign.top),
              popupBuilder: (BuildContext context, Marker marker) {
                String direc = '';

                _getPlace(marker).then((value) => _address = value);
                for (Marcador m in marcadores) {
                  if (m.marker == marker) {
                    direc = m.direccion.toString();
                    detallesEstacionesCarga = m.detalles;
                  }
                }
                //pr.hide();
                //_progressDialog.dismissProgressDialog(context);

                return ExamplePopup2(
                    marker,
                    _mapController,
                    changeColor,
                    marcadores,
                    _address,
                    loading,
                    widget.token,
                    listCharges!,
                    direcciones,
                    direc,
                    detallesEstacionesCarga!);
              }),
        ),
      ],
    ));
  }

//Widget principal donde se contruyen todos los anteriores widgets
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => await _onWillPopScope(context),
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          centerTitle: true,
          title: const Text('MAPA'),
          backgroundColor: Color.fromRGBO(48, 128, 210, 1),
          actions: <Widget>[
            IconButton(
              icon: Image.asset(
                'assets/images/icons8-recargar-24.png',
                color: Colors.white,
              ),
              onPressed: () {
                // Aquí puedes iniciar el proceso de recarga al presionar el ícono
                _handleRefresh();
              },
            ),
          ],
        ),
        body: Container(child: Stack(children: <Widget>[_mapa()])),
      ),
    );
  }
}
