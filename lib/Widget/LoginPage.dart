import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_map/plugin_api.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mivilsoft/Widget/Mapa.dart';
import 'package:mivilsoft/Widget/SignUp.dart';
import 'package:mivilsoft/Widget/WelcomePage.dart';
import 'package:mivilsoft/models/tenant_model.dart';
import 'package:mivilsoft/services/tenant.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:progress_dialog2/progress_dialog2.dart';

import '../models/autentication_model.dart';
import '../services/Autentication.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Login _login = Login.init();
  SharedPreferences? dataLogin;
  //ProgressDialog _progressDialog = ProgressDialog();
  TextEditingController? _textEditingController,
      _textEditingControllerPassword,
      _textEditingControllerTenant;
  int? statusCode;
  String? token;
  List<Marker>? marker;
  bool loading = true;
  bool check = false;
  bool _obscureText = true;
  List<Result>? listTenants;
  List<String> listSudomain = [];
  String? sudomain;
  String? selectedCombo;
  late ProgressDialog pr;

  @override
  void initState() {
    //Mostrador de carga para los datos del dominio
    super.initState();
    pr = new ProgressDialog(context, showLogs: true);
    pr.style(message: 'Cargando...');
    pr.show();
    /*_progressDialog.showProgressDialog(context,
        dismissAfter: Duration(seconds: 5),
        textToBeDisplayed: 'Cargando datos...', onDismiss: () {
      //things to do after dismissing -- optional
    });*/
    cargarDataLogin();
    cargarTenants();
  }

  //Funciones
//Llamada al servicio de obtener el dominio y cargarlos e una lista
  cargarTenants() {
    getTenant().then((value) async {
      listTenants = value;
      if (listTenants != null) {
        for (Result r in listTenants!) {
          sudomain = r.subdomain.toString();
          listSudomain.add(sudomain!);
        }
      }
      setState(() {
        listSudomain;
      });
      await Future.delayed(Duration(seconds: 10));
      pr.hide();
      //_progressDialog.dismissProgressDialog(context);
    });
  }

//Obtiene los datosque se ingresa al autenticarse y los guarda con sharedpreferences
  cargarDataLogin() async {
    dataLogin = await SharedPreferences.getInstance();
    if (dataLogin != null) {
      setState(() {
        _login.email = dataLogin!.getString("email");
        _textEditingController = TextEditingController(text: _login.email);
        _login.password = dataLogin!.getString("password");
        _textEditingControllerPassword =
            TextEditingController(text: _login.password);
        _login.tenant = dataLogin!.getString("tenat");
        selectedCombo = _login.tenant;
      });
    } else {
      CircularProgressIndicator();
    }
  }

//Funció que solicita el error y de acuerdo a eso muestra una alerta explicando el error
  Future<void> alertError(int error) {
    String text;
    text = getError(error);
    return showDialog<void>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Notificación"),
            content: Text(text),
            actions: [
              TextButton(
                style: TextButton.styleFrom(
                  textStyle: Theme.of(context).textTheme.labelLarge,
                ),
                child: const Text('Ok'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  //Widgets
  //Se crea el logo de la empresa
//Este método devuelve un widget que muestra el logo de la empresa en la parte superior de la pantalla de detalles.
  Widget _logo() {
    return Container(
        margin: EdgeInsets.fromLTRB(0, 80, 0, 0),
        alignment: Alignment.center,
        // Alto del contenedor
        child: Stack(children: [
          Positioned(
            child: Image.asset(
              'assets/images/logo_sin_fondo.png',
              width: 250,
              height: 250,
            ),
          ),
        ]));
  }

//Widget creado para crear un lista de los dominios y ponerlos en un comboBox
  Widget comboTenants() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "Dominio",
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          DropdownButtonHideUnderline(
            child: DropdownButton2(
              alignment: Alignment.center,
              isExpanded: true,
              buttonHeight: 50,
              buttonWidth: 220,
              buttonPadding: const EdgeInsets.symmetric(horizontal: 20),
              buttonDecoration: BoxDecoration(
                color: Colors.white,
              ),
              hint: Row(
                children: const [
                  Icon(
                    Icons.list,
                    size: 16,
                    color: Colors.black,
                  ),
                  SizedBox(
                    width: 4,
                  ),
                  Expanded(
                    child: Text(
                      'Select ',
                      style: TextStyle(
                        fontSize: 14,
                        //fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ],
              ),
              items: listSudomain
                  .map((item) => DropdownMenuItem<String>(
                        value: item,
                        child: Text(
                          item,
                          style: const TextStyle(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                          overflow: TextOverflow.ellipsis,
                        ),
                      ))
                  .toList(),
              value: selectedCombo,
              onChanged: (value) {
                setState(() {
                  selectedCombo = value as String;
                  _login.tenant = selectedCombo;
                });
              },
              icon: const Icon(
                Icons.arrow_forward_ios_outlined,
                size: 15,
                color: Color.fromARGB(255, 0, 0, 0),
              ),
              itemHeight: 40,
              dropdownPadding: EdgeInsets.symmetric(horizontal: 20),
              dropdownMaxHeight: 200,
              dropdownWidth: 200,
              dropdownDecoration: BoxDecoration(
                borderRadius: BorderRadius.circular(14),
                color: Color.fromARGB(255, 142, 176, 250),
              ),
              scrollbarThickness: 10,
              scrollbarAlwaysShow: false,
            ),
          ),
        ],
      ),
    );
  }

//Widget que crea rea un widget que contiene dos campos de entrada de texto para el usuario y la contraseña,
// junto con un widget de selección de "Tenant" (dominio).
  Widget _emailPasswordWidget() {
    return Column(
      children: <Widget>[
        entryField("Correo", 'icons8-usuario-30.png'),
        entryField("Contraseña", 'icons8-bloquear-30.png', isPassword: true),
        comboTenants()
      ],
    );
  }

//Widget que se encarga de crear un campo de entrada de texto con un título,
//una imagen de prefijo y la capacidad de ocultar o mostrar la contraseña según sea necesario.
  Widget entryField(String title, String imagen, {bool isPassword = false}) {
    String enlace = 'assets/images/';
    return Container(
      margin: EdgeInsets.symmetric(vertical: 15, horizontal: 40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            obscureText: isPassword == false ? isPassword : _obscureText,
            controller: isPassword == false
                ? _textEditingController
                : _textEditingControllerPassword,
            decoration: InputDecoration(
                prefixIcon: Image.asset(enlace + imagen),
                suffixIcon: isPassword == true
                    ? GestureDetector(
                        onTap: () {
                          setState(() {
                            _obscureText = !_obscureText;
                          });
                        },
                        child: Icon(
                          _obscureText
                              ? Icons.visibility
                              : Icons.visibility_off,
                        ),
                      )
                    : null,
                border: InputBorder.none,
                fillColor: Colors.white,
                filled: true),
            onChanged: (Value) => setState(
              () => title == 'Correo'
                  ? _login.email = Value
                  : _login.password = Value,
            ),
          )
        ],
      ),
    );
  }

//Widget que crea un boton para enviar todo el formulario al servicio de autenticarse
  Widget _submitButton(BuildContext context) {
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        backgroundColor: Color.fromARGB(255, 108, 175, 226), // background
        minimumSize: Size(350, 50),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(25.0),
        ), // foreground
      ),
      onPressed: () {
        setState(() {
          loading = true;
        }); // Establecer loading a true antes de la llamada a la API

        fetchPost(_login).then((value) {
          setState(() {
            token = value!.token;
            statusCode = value.statusCode;
            loading = false;
          });
          if (loading) {
            CircularProgressIndicator();
          } else {
            if (token != null) {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => Mapa2(token!)));
              //alert(context, Mapa2(token!));
              dataLogin!.setString("email", _login.email!);
              dataLogin!.setString("password", _login.password!);
              dataLogin!.setString("tenat", _login.tenant!);
            } else {
              alertError(statusCode!);
            }
          }
        });
      },
      child:
          Text('Ingresar', style: TextStyle(fontSize: 20, color: Colors.white)),
    );
  }

//Wiget que contiene un separador o una linea divisora
  Widget _divider() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20, horizontal: 30),
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 20,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 0),
              child: Divider(
                thickness: 2,
              ),
            ),
          ),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

//ste widget crea un enlace para registrar una cuenta.
  Widget _createAccountLabel() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => SignUp()));
      },
      child: Container(
        alignment: Alignment.bottomCenter,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'No tienes una cuenta?',
              style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              'Registrar',
              style: TextStyle(
                  color: Color.fromARGB(255, 39, 106, 173),
                  fontSize: 15,
                  fontWeight: FontWeight.w600),
            ),
          ],
        ),
      ),
    );
  }

// Este widget muestra el título de la página en la parte superior.
  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'Mivil',
          style: GoogleFonts.portLligatSans(
            textStyle: Theme.of(context).textTheme.headline1,
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Colors.white,
          ),
          children: [
            TextSpan(
              text: 'Soft',
              style: TextStyle(color: Colors.black, fontSize: 30),
            ),
            TextSpan(
              text: '  S.A',
              style: TextStyle(color: Colors.white, fontSize: 30),
            ),
          ]),
    );
  }

//Widget principal donde se contruyen todos los anteriores widgets
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => WelcomePage()));
        return false;
      },
      child: Scaffold(
          body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(5)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                  color: Colors.grey.shade200,
                  offset: Offset(2, 4),
                  blurRadius: 5,
                  spreadRadius: 2)
            ],
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.center,
                colors: [
                  Color.fromRGBO(44, 62, 80, 0.75),
                  Color.fromARGB(255, 219, 230, 238)
                ])),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _logo(),
              _emailPasswordWidget(),
              SizedBox(height: 20),
              _submitButton(context),
              _divider(),
              _createAccountLabel(),
              //to do recuperar contraseña
            ],
          ),
        ),
      )),
    );
  }
}
