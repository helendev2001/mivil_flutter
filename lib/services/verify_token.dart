import 'package:http/http.dart' as http;
import 'dart:convert';

// Esta función se utiliza para validar la respuesta de un reCAPTCHA generado al registrarse.
// Toma el `responseToken` del reCAPTCHA como entrada y devuelve un valor booleano que indica
// si la validación fue exitosa (true) o no (false).
Future<bool> validateCaptcha(String responseToken) async {
  bool verify = false;
  final secretKey =
      '6LfNXh0lAAAAAGcEY_mnPYb0LefTqjV6GuD-st3n'; // Clave secreta del reCAPTCHA
  final url =
      'https://www.google.com/recaptcha/api/siteverify'; // URL de verificación del reCAPTCHA

  final response = await http.post(
    Uri.parse(url),
    body: {
      'secret': secretKey,
      'response': responseToken,
    },
  );

  final responseData = json.decode(
      response.body); // Decodifica la respuesta JSON del servidor reCAPTCHA

  if (responseData['success']) {
    // Verifica si la respuesta del reCAPTCHA indica éxito (success: true)
    verify = true;
  }
  return verify; // Devuelve un valor booleano que indica si la validación del reCAPTCHA fue exitosa o no.
}
