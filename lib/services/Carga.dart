import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mivilsoft/models/charging_model.dart';

// Esta función realiza una solicitud HTTP GET para obtener datos de estaciones de carga.
// Toma un token de autenticación como entrada y devuelve una lista de objetos Result,
// que representan información sobre estaciones de carga.
Future<List<Result>?> getDataCharge(String token) async {
  List<Result>? cargas;
  Charging? carga;

  try {
    final response = await http.get(
      Uri.parse(
        // URL de la API para obtener datos de estaciones de carga
        'http://192.168.100.22/v1/api/charging-stations?Issuer=true&WithSite=true&WithSiteArea=true&WithUser=true&Limit=50&SortFields=id'
      ),
      headers: <String, String>{
        'Accept': '*/*',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token', // Agrega el token en el encabezado de autorización
      },
    );

    if (response.statusCode == 200) {
      // Verifica si la respuesta tiene un estado HTTP 200, lo que significa éxito
      carga = Charging.fromJson(jsonDecode(response.body));
      cargas = carga.result; // Extrae la lista de estaciones de carga de la respuesta
    }
    return cargas; // Devuelve la lista de estaciones de carga (puede ser nula si hay un error).
  } catch (e) {
    print("Error al cargar markers " + e.toString());
  }
}
