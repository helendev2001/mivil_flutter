import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mivilsoft/models/autentication_model.dart';
import 'package:mivilsoft/models/token_model.dart';

// Esta función realiza una solicitud HTTP POST a la URL especificada para autenticar
// a un usuario utilizando los datos de inicio de sesión proporcionados.
// Devuelve un objeto Token que contiene el token de autenticación y el código de estado HTTP.
Future<Token?> fetchPost(Login login) async {
  String? token;
  int? statusCode;
  String url = 'http://192.168.100.22/v1/auth/signin'; // URL de la API

  try {
    final response = await http
        .post(Uri.parse(url),
            headers: <String, String>{
              'Accept': '*/*',
              'Content-Type': 'application/json',
            },
            body: jsonEncode(login)) // Codifica los datos del inicio de sesión en JSON
        .timeout(const Duration(seconds: 30)); // Establece un tiempo límite de 30 segundos para la solicitud

    if (response.statusCode == 200) {
      // Verifica si la respuesta tiene un estado HTTP 200, lo que significa éxito
      token = response.body.replaceAll('{"token":"', '').replaceAll('"}', '');
      statusCode = response.statusCode;
    } else {
      statusCode = response.statusCode; // En caso de error, captura el código de estado HTTP
    }
  } catch (e) {
    print(e.toString());
  }

  return Token(statusCode: statusCode, token: token);
  // Retorna un objeto Token que contiene el código de estado y el token (puede ser nulo).
}

// Esta función toma un código de estado HTTP como entrada y devuelve un mensaje de error
// asociado con ese código de estado.
String getError(int? statusCode) {
  String? text;
  switch (statusCode) {
    case 404:
      text = "Usuario o contraseña incorrecta";
      break;
    case 500:
      text = "No se puede acceder al sitio";
      break;
    case 520:
      text = "Error de conexión";
      break;
    case 590:
      text = "Cuenta bloqueada";
      break;
    case 592:
      text = "Cuenta pendiente";
      break;
    case 593:
      text = "Cuenta bloqueada";
      break;
    case 594:
      text = "Cuenta inactiva";
      break;
  }
  return text!; // Devuelve el mensaje de error correspondiente (puede ser nulo si no hay un mensaje definido).
}
