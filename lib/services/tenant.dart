import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:mivilsoft/models/tenant_model.dart';
// Esta función se utiliza para obtener el dominio de la organización mediante una solicitud HTTP POST.
// Primero, realiza una solicitud de inicio de sesión para obtener un token de autenticación.
// Luego, utiliza ese token para realizar una solicitud HTTP GET para obtener información sobre el dominio de la organización.
// Devuelve una lista de objetos Result que representan información sobre el dominio.
Future<List<Result>?> getTenant() async {
  String? token;
  Tenant? tenant;
  List<Result>? resultTenant;

  try {
    final response = await http
        .post(Uri.parse('http://192.168.100.22/v1/auth/signin'), // URL de inicio de sesión para obtener el token
            headers: <String, String>{
              'Accept': '*/*',
              'Content-Type': 'application/json',
            },
            body: jsonEncode(<String, dynamic>{
              "email": "hlmoreta@espe.edu.ec",
              "password": "Lis_2001",
              "tenant": "",
              "acceptEula": true
            }))
        .timeout(const Duration(seconds: 10)); // Establece un tiempo límite de 10 segundos para la solicitud

    if (response.statusCode == 200) { // Si la solicitud de inicio de sesión fue exitosa
      token = response.body.replaceAll('{"token":"', '').replaceAll('"}', '');
      final data = await http.get(
        Uri.parse(
            'http://192.168.100.22/v1/api/tenants?WithLogo=false&WithComponents=false&WithAddress=false&Limit=100&Skip=0&SortFields=id&OnlyRecordCount=false'),
        headers: <String, String>{
          'Accept': '*/*',
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token', // Agrega el token en el encabezado de autorización
        },
      );

      if (data.statusCode == 200) { // Si la solicitud para obtener información del dominio fue exitosa
        tenant = Tenant.fromJson(jsonDecode(data.body));
        resultTenant = tenant.result; // Extrae la información del dominio de la organización
      }

      return resultTenant; // Devuelve la lista de resultados que representan la información del dominio.
    }
  } catch (e) {
    print(e.toString());
  }
}
