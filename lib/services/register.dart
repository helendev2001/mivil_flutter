import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:mivilsoft/models/register_model.dart';

// Este servicio se utiliza para registrar un usuario mediante una solicitud HTTP POST.
// Toma un objeto Register como entrada que contiene los detalles del usuario a registrar.
// Devuelve un código de estado HTTP que indica el resultado de la operación de registro.
Future<int> registerUser(Register user) async {
  int statusCode = 0;
  print(user.toJson());
  user.locale = "en-US";

  // URL de la API para registrar un usuario
  String url = 'http://192.168.100.22/v1/auth/signon';

  try {
    final response = await http
        .post(
          Uri.parse(url),
          headers: <String, String>{
            'Accept': '*/*',
            'Content-Type': 'application/json',
          },
          body: jsonEncode(
              user), // Codifica el objeto de usuario para el registro
        )
        .timeout(const Duration(
            seconds:
                30)); // Establece un tiempo límite de 30 segundos para la solicitud

    if (response.statusCode == 200) {
      // Verifica si la respuesta tiene un estado HTTP 200, lo que significa que el usuario se registró exitosamente
      statusCode = response.statusCode;
    } else {
      statusCode = response
          .statusCode; // Captura el código de estado HTTP en caso de error
    }
  } catch (e) {
    print(e.toString());
  }

  return statusCode;
  // Devuelve el código de estado HTTP que indica el resultado del registro.
  // Si la llamada no fue exitosa, el código de estado será diferente de 200.
}
